public OnPlayerText(playerid, text[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		format(message, sizeof(message), "%s mówi: %s", player[playerid][gamenick], text);
		ProxDetector(9.0, playerid, message, -1);
	}
	return 0;
}

public OnPlayerCommandPerformed(playerid, cmdtext[], success) 
{ 
  return SendClientMessage(playerid, -1, "Komenda nie istnieje. Użyj: /pomoc");
}

CMD:b(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		new ooc[100];
		if(sscanf(params, "s[100]", ooc))
		{
			SendClientMessage(playerid, -1, "Użycie: /b [tekst]");
			return 1;
		}
		else
		{
			format(message, sizeof(message), "(( [%d] %s: %s ))", playerid, player[playerid][gamenick], ooc);
		    ProxDetector(9.0, playerid, message, 0xCCCCCCAA);
		}
	}
	return 1;
}

CMD:s(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		new shout[100];
		if(sscanf(params, "s[100]", shout))
		{
			SendClientMessage(playerid, -1, "Użycie: /s [tekst]");
			return 1;
		}
		else
		{
			format(message, sizeof(message), "%s szepcze: %s", player[playerid][gamenick], shout);
		    ProxDetector(3.0, playerid, message, -1);
		}
	}
	return 1;
}

CMD:szept(playerid, params[]) return cmd_s(playerid, params);

CMD:k(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		new kk[100];
		if(sscanf(params, "s[100]", kk))
		{
			SendClientMessage(playerid, -1, "Użycie: /k [tekst]");
			return 1;
		}
		else
		{
			format(message, sizeof(message), "%s krzyczy: %s!!!", player[playerid][gamenick], kk);
		    ProxDetector(14.0, playerid, message, -1);
		}
	}
	return 1;
}

CMD:krzyk(playerid, params[]) return cmd_k(playerid, params);

CMD:me(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		new me[100];
		if(sscanf(params, "s[100]", me))
		{
			SendClientMessage(playerid, -1, "Użycie: /me [tekst]");
			return 1;
		}
		else
		{
			format(message, sizeof(message), "* %s %s", player[playerid][gamenick], me);
		    ProxDetector(9.0, playerid, message, 0xC2A2DAAA);
		}
	}
	return 1;
}

CMD:do(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		new message[128];
		new doo[100];
		if(sscanf(params, "s[100]", doo))
		{
			SendClientMessage(playerid, -1, "Użycie: /do [tekst]");
			return 1;
		}
		else
		{
			format(message, sizeof(message), "* %s (( %s ))", doo, player[playerid][gamenick]);
		    ProxDetector(9.0, playerid, message, 0xC2A2DAAA);
		}
	}
	return 1;
}


stock ProxDetector(Float:radi, playerid, string[], color)
{
    new Float:x, Float:y, Float:z;
    GetPlayerPos(playerid, x, y, z);
    for(new i=0; i<LIMIT_PLAYERS; i++)
    {
    	if(IsPlayerConnected(playerid))
    	{
	        if(IsPlayerInRangeOfPoint(i, radi, x, y, z)) 
        	{
            	SendClientMessage(i, color, string);
	    	}
    	}
    }
}