new TimerReason;

forward hideBox();
public hideBox()
{
	TextDrawHideForAll(TextDrawPenalty);
}

stock setPenalty(type, adminid, playerid, time, reason[])
{
	new penalty[48];
	new time_text[16];
	new player_message[128];

	if(time >= 17) time=16;

	if(time == -1)
	{
		time_text = "Na zawsze";
	}
	else if(time == 0)
	{
		time_text = "0 h";
	}
	else if(time > 0 && time < 10)
	{
		format(time_text, sizeof(time_text), "%d h", time);
	}
	else if(time == 10)
	{
		time_text = "1 dzien";
	}
	else if(time > 10 && time < 17)
	{
		format(time_text, sizeof(time_text), "%d dni", time-10);
	}

	if(type == 0)
	{
		format(penalty, sizeof(penalty), "Kick");
		format(player_message, sizeof(player_message), "Zostałeś wyrzucony z serwera przez %s. Powód: %s", player[adminid][gamenick], reason);
		SendClientMessage(playerid, -1, player_message);
	}
	else if(type == 1)
	{
		format(penalty, sizeof(penalty), "Warn");
	}
	else if(type == 2)
	{
		format(penalty, sizeof(penalty), "Admin jail (%s)", time_text);
		format(player_message, sizeof(player_message), "Trafiasz do AJ przez %s na %s. Powód: %s", player[adminid][gamenick], time_text, reason);
		SendClientMessage(playerid, -1, player_message);
	}
	else if(type == 3)
	{
		format(penalty, sizeof(penalty), "Blokada postaci (%s)", time_text);
	}
	else if(type == 4)
	{
		format(penalty, sizeof(penalty), "Ban");
	}
	else if(type == 5)
	{
		format(penalty, sizeof(penalty), "Blokada czaty OOC (%s)", time_text);
	}
	else if(type == 6)
	{
		format(penalty, sizeof(penalty), "Blokada biegania (%s)", time_text);
	}
	else if(type == 7)
	{
		format(penalty, sizeof(penalty), "Blokada broni (%s)", time_text);
	}
	else if(type == 8)
	{
		format(penalty, sizeof(penalty), "Blokada prowadzenia pojazdow (%s)", time_text);
	}

	new message[128];
	format(message, sizeof(message), "~r~~h~%s~n~Gracz:  %s~n~Nadajacy: %s~n~~y~~h~%s", penalty, player[playerid][gamenick], player[adminid][gamenick], reason);
	TextDrawSetString(TextDrawPenalty, message);
	TextDrawShowForAll(TextDrawPenalty);
}

CMD:duty(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		if(player[playerid][adminlvl] > 0)
		{
			if(player[playerid][duty] != true)
			{
				new tmp_text[64];
				player[playerid][duty] = true;
				format(player[playerid][gamenick], 24, "%s", player[playerid][globalname]);
				format(tmp_text, sizeof(tmp_text), "%s (%d)\n{%s}[%s]", player[playerid][gamenick], playerid, player[playerid][admincolor], player[playerid][admintitle]);
				Update3DTextLabelText(player[playerid][nick], -1, tmp_text);
				SendClientMessage(playerid, -1, "Wchodzisz na służbę");
			}	
			else 
			{
				new tmp_text[64], tmp[24];
				player[playerid][duty] = false;
				format(tmp, sizeof(tmp), "%s %s", player[playerid][name], player[playerid][surname]);
				player[playerid][gamenick] = tmp;
				format(tmp_text, sizeof(tmp_text), "%s (%d)", player[playerid][gamenick], playerid);
				Update3DTextLabelText(player[playerid][nick], -1, tmp_text);
				SendClientMessage(playerid, -1, "Schodzisz ze służby");
			}		
		}
	}
	return 1;
}


CMD:kick(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		if(player[playerid][adminlvl] > 0)
		{
			if(player[playerid][duty] == true)
			{
				new reason[32];
				new kick_player;
				if(sscanf(params, "ds[32]", kick_player, reason))
				{
					SendClientMessage(playerid, -1, "Użycie: /kick [id gracza] [powód]");
					return 1;
				}
				else
				{
					if(player[kick_player][isLogin] == true)
					{
						setPenalty(0, playerid, kick_player, 0, reason);
						KillTimer(TimerReason);
						TimerReason = SetTimer("hideBox", 5000, false);
						return 1;
					}
					else
					{
						SendClientMessage(playerid, -1, "Gracz nie jest zalogowany.");
						return 1;
					}
				}
			}
			else
			{
				SendClientMessage(playerid, -1, "Nie jestes na sluzbie stroza");
				return 1;
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "Nie posiadasz uprawnień do użycia tej komendy.");
			return 1;
		}
	}
	return 1;
}

CMD:aj(playerid, params[])
{
	if(player[playerid][isLogin] == true)
	{
		if(player[playerid][adminlvl] > 1)
		{
			if(player[playerid][duty] == true)
			{
				new reason[32];
				new aj_time, aj_player;
				if(sscanf(params, "dds[32]", aj_player, aj_time, reason))
				{
					SendClientMessage(playerid, -1, "Użycie: /aj [id gracza] [czas] [powód]");
					return 1;
				}
				else
				{
					if(player[aj_player][isLogin] == true)
					{
						setPenalty(2, playerid, aj_player, aj_time, reason);
						KillTimer(TimerReason);
						TimerReason = SetTimer("hideBox", 5000, false);
						return 1;
					}
					else
					{
						SendClientMessage(playerid, -1, "Gracz nie jest zalogowany.");
						return 1;
					}
				}
			}
			else
			{
				SendClientMessage(playerid, -1, "Nie jestes na sluzbie stroza");
				return 1;
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "Nie posiadasz uprawnień do użycia tej komendy.");
			return 1;
		}
	}
	return 1;
}