#define DIALOG_LOGIN 1
#define DIALOG_NOACCOUNT 2
#define DIALOG_LOGIN_ERROR 3

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    if(dialogid == DIALOG_LOGIN)
    {
        if(response)
        {
        	new buffer[128];
	        mysql_format(Connection, buffer, sizeof(buffer), "SELECT username, password, salt FROM `rp_users` WHERE `uid` = '%d' LIMIT 1", player[playerid][gid]);
	    	new Cache:result = mysql_query(Connection, buffer);
			if(cache_num_rows() > 0)
			{
			    new password[64], salt[24];
			    cache_get_value_index(0, 0, player[playerid][globalname], 24);
				cache_get_value_index(0, 1, password);
				cache_get_value_index(0, 2, salt);

				if(passwordVerify(password, inputtext, salt))
				{
                    cache_delete(result);
                    mysql_format(Connection, buffer, sizeof(buffer), "SELECT rang_id, title, color FROM `rp_administration` WHERE `gid` = '%d' LIMIT 1", player[playerid][gid]);
                    result = mysql_query(Connection, buffer);
                    if(cache_num_rows() > 0)
                    {
                        cache_get_value_index_int(0, 0, player[playerid][adminlvl]);
                        cache_get_value_index(0, 1, player[playerid][admintitle], 48);
                        cache_get_value_index(0, 2, player[playerid][admincolor], 8);
                    }
                    else
                    {
                        player[playerid][adminlvl] = 0;
                    }
					player[playerid][isLogin] = true;
                    format(salt, sizeof(salt), "%s %s", player[playerid][name], player[playerid][surname]);
                    player[playerid][gamenick] = salt;
					SetPlayerPos(playerid, player[playerid][posX], player[playerid][posY], player[playerid][posZ]);
					SetPlayerSkin(playerid, player[playerid][skin]);
					SetPlayerHealth(playerid, player[playerid][hp]);
					SpawnPlayer(playerid);
				}
				else
				{
					SendClientMessage(playerid, -1, "Wprowadzono niepoprawne hasło.");
					Kick(playerid);
				}
			}
			cache_delete(result);
        }
        else
        {
            Kick(playerid);
        }
        return 1;
    }
    else if(dialogid == DIALOG_NOACCOUNT)
    {
    	if(response)
    	{
    		 Kick(playerid);
    	}
    	else 
    	{
    		 Kick(playerid);
    	}
    	return 1;
    }
 
    return 0;
}