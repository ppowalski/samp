#define LIMIT_PLAYERS 100

enum PLAYER_DATA
{
	bool:isLogin,
	uid,
	gid,
	name[24],
	surname[24],
	globalname[24],
	age,
	skin,
	Float:hp,
	Float:posX,
	Float:posY,
	Float:posZ,
	Text3D:nick,
	gamenick[24],
	bool:duty,
	adminlvl,
	admincolor[8],
	admintitle[48]
}

new player[MAX_PLAYERS][PLAYER_DATA];

passwordVerify(passwordGood[], passwordUser[], salt[])
{
	new password[128];
	format(password, sizeof(password), "%s%s", MD5_Hash(salt), MD5_Hash(passwordUser));
	password = MD5_Hash(password);

	if(!strcmp(passwordGood, password))
	{
		return 1;
	}
	return 0;
}