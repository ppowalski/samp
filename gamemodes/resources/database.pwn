#define dbHost "localhost"
#define dbUser "root"
#define dbPassword ""
#define dbName "rp"

new MySQL:Connection;

Connect()
{
	new MySQLOpt: option_id = mysql_init_options();
	mysql_set_option(option_id, AUTO_RECONNECT, true);

	Connection = mysql_connect(dbHost, dbUser, dbPassword, dbName, option_id);

	if(Connection == MYSQL_INVALID_HANDLE || mysql_errno(Connection) != 0)
	{
		print("Nie udalo się połaczyć z baza danych.");
		SendRconCommand("exit");
		return 1;
	} 
	else
	{
		print("Poprawnie połączono z bazą danych");
	}
	return 1;
}
