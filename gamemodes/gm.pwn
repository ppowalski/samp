#include <a_samp>
#include <a_mysql>
#include <sscanf2>
#include <zcmd>
#include <md5>

new Text:TextDrawPenalty; 

#include "resources/database.pwn"
#include "resources/player.pwn"
#include "resources/dialogs.pwn"
#include "resources/chat.pwn"
#include "resources/admin.pwn"

main()
{}

public OnGameModeInit()
{
	Connect();
	SendRconCommand("mapname Los Santos");
	ShowNameTags(0);
    ShowPlayerMarkers(0);
   	AllowInteriorWeapons(1);
	EnableStuntBonusForAll(0);

	TextDrawPenalty = TextDrawCreate(14.000000, 280.000000, "Test");
	TextDrawBackgroundColor(TextDrawPenalty, 255);
	TextDrawFont(TextDrawPenalty, 1);
	TextDrawLetterSize(TextDrawPenalty, 0.300000, 1.200000);
	TextDrawColor(TextDrawPenalty, -1);
	TextDrawSetOutline(TextDrawPenalty, 1);
	TextDrawSetProportional(TextDrawPenalty, 1);
	TextDrawShowForAll(TextDrawPenalty);
	return 1;
}

public OnPlayerConnect(playerid)
{
	new buffer[128];
	new tmp[MAX_PLAYER_NAME];

	GetPlayerName(playerid, tmp, sizeof(tmp));

 	player[playerid][name]=tmp;
 	player[playerid][surname]=tmp;
	strdel(player[playerid][surname], 0, strfind(tmp, "_")+1);
	strdel(player[playerid][name], strfind(tmp, "_"), sizeof(tmp));

	mysql_format(Connection, buffer, sizeof(buffer), "SELECT * FROM `rp_characters` WHERE `name` = '%e' AND `surname` = '%e' LIMIT 1", player[playerid][name], player[playerid][surname]);
    new Cache:result = mysql_query(Connection, buffer);
    if(cache_num_rows() > 0)
    {
   	 	ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Logowanie", "Witaj na serwerze Patryk Role Play.\nTwoja posta� zosta�a znaleziona w naszej bazie danych.\nWprowad� has�o aby si� zalogowa�.", "Zaloguj", "Anuluj");
		cache_get_value_index_int(0, 0, player[playerid][uid]);
		cache_get_value_index_int(0, 1, player[playerid][gid]);
		cache_get_value_index_int(0, 4, player[playerid][age]);
		cache_get_value_index_int(0, 5, player[playerid][skin]);
		cache_get_value_index_float(0, 6, player[playerid][hp]);
		cache_get_value_index_float(0, 7, player[playerid][posX]);
		cache_get_value_index_float(0, 8, player[playerid][posY]);
		cache_get_value_index_float(0, 9, player[playerid][posZ]);
        cache_delete(result);
    }
    else
    {
        ShowPlayerDialog(playerid, DIALOG_NOACCOUNT, DIALOG_STYLE_MSGBOX, "Logowanie", "Witaj na serwerze Patryk Role Play.\nPosta� o podanym nicku nie zosta�a znaleziona w naszej bazie danych.\nUdaj si� na stron� www.google.com i utw�rz posta�.", "Ok", "Anuluj");
    }
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	Delete3DTextLabel(player[playerid][nick]);
    return 1;
}

public OnPlayerSpawn(playerid)
{
	if(player[playerid][isLogin] == false)
	{
	    Kick(playerid);
	}
	else
	{
		SetPlayerPos(playerid, player[playerid][posX], player[playerid][posY], player[playerid][posZ]);
		SetPlayerSkin(playerid, player[playerid][skin]);
		SetPlayerHealth(playerid, player[playerid][hp]);
 		new player_nick[MAX_PLAYER_NAME];
	    format(player_nick, sizeof(player_nick), "%s (%d)", player[playerid][gamenick], playerid);
		player[playerid][nick] = Create3DTextLabel(player_nick , 0xCCCCCCFF, 30.0, 40.0, 50.0, 10.0, 0);
  		Attach3DTextLabelToPlayer(player[playerid][nick], playerid, 0.0, 0.0, 0.16);
  		SetPlayerColor(playerid, 0xCCCCCCFF);
	}
    return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	SetSpawnInfo( playerid, 0, player[playerid][skin], player[playerid][posX], player[playerid][posY], player[playerid][posZ], 0.0, 0, 0, 0, 0, 0, 0 );
	SpawnPlayer(playerid);
	return 1;
}

